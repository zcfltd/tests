/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.tests.httpserver.http.server;


import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;


/**
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
public class MyHTTPServer {

	private static HttpServer server = null;
	private static WebSocketServer wsServer = null;

	@SuppressWarnings( "UseOfSystemOutOrSystemErr" )
	public static void main( String[] args ) throws IOException {

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {

			if ( server != null ) {
				server.stop(0);
			}

			if ( wsServer != null ) {
				try {
					wsServer.stop(0);
				} catch ( InterruptedException ex ) {
					Logger.getLogger(MyHTTPServer.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

		}));

		server = HttpServer.create(new InetSocketAddress(4321), 0);
		HttpContext context = server.createContext("/java");

		context.setHandler((HttpExchange he) -> {

			URI requestURI = he.getRequestURI();

			System.out.println("Request Method: " + he.getRequestMethod());
			System.out.println("   Request URI: " + requestURI.toString());

			he.sendResponseHeaders(200, -1);
			he.close();

			if ( "/java/stop".equals(requestURI.toString()) ) {

				server.stop(3);

				if ( wsServer != null ) {
					try {
						wsServer.stop(0);
					} catch ( InterruptedException ex ) {
						Logger.getLogger(MyHTTPServer.class.getName()).log(Level.SEVERE, null, ex);
					}
				}

			}

		});

		server.start();

		wsServer = new WebSocketServer(new InetSocketAddress(4321)) {

			@Override
			public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
				System.out.println("WebSocketServer.onClose");
			}

			@Override
			public void onError( WebSocket conn, Exception ex ) {
				System.out.println("WebSocketServer.onError: " + ex.getMessage());
			}

			@Override
			public void onMessage( WebSocket conn, String message ) {
				System.out.println("WebSocketServer.onMessage: " + message);
			}

			@Override
			public void onOpen( WebSocket conn, ClientHandshake handshake ) {
				System.out.println("WebSocketServer.onOpen");
			}

		};
		
		wsServer.start();

	}

}
