/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.tests.scp.from;


import java.util.logging.Level;
import java.util.logging.Logger;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.xfer.FileSystemFile;


/**
 * Getting a file emulating the following command:
 * <PRE>
 * scp -i /Users/claudiorosati/Documents/Keys/minibemskey.pem ec2-user@52.17.172.149:/home/ec2-user/minibems-ws-server/logs/minibems-ws-server-0.log /Users/claudiorosati/Desktop/
 * </PRE>
 *
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
public class ScpFrom {
	
	private static final Logger LOGGER = Logger.getLogger(ScpFrom.class.getName());

	/**
	 * @param args the command line arguments
	 */
	public static void main( String[] args ) {
		
		try ( SSHClient client = new SSHClient() ) {
			
			client.loadKnownHosts();
			client.connect("52.17.172.149");
			client.authPublickey("ec2-user", "/Users/claudiorosati/Documents/Keys/minibemskey.pem");
			
			try {
				client.newSCPFileTransfer().download("/home/ec2-user/minibems-ws-server/logs/minibems-ws-server-0.log", new FileSystemFile("/Users/claudiorosati/Desktop/"));
			} finally {
				client.disconnect();
			}
			
		} catch ( Exception ex ) {
			LOGGER.log(Level.SEVERE, null, ex);
		}
		
	}
	
}
