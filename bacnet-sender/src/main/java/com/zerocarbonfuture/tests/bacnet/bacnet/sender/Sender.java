/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.tests.bacnet.bacnet.sender;


import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.event.DeviceEventAdapter;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.service.unconfirmed.WhoIsRequest;
import com.serotonin.bacnet4j.transport.Transport;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
public class Sender {

	private static final Logger LOGGER = Logger.getLogger(Sender.class.getName());

	@SuppressWarnings( { "UseOfSystemOutOrSystemErr", "SleepWhileInLoop", "CallToThreadYield" } )
	public static void main( String[] args ) {

		IpNetwork network = new IpNetwork();
		Transport transport = new Transport(network);
		LocalDevice localDevice = new LocalDevice(45678, transport);

		localDevice.getEventHandler().addListener(new DeviceEventAdapter() {
			@Override
			@SuppressWarnings( "UseOfSystemOutOrSystemErr" )
			public void iAmReceived( RemoteDevice d ) {
				System.out.println(MessageFormat.format("\n*** 'I AM' received from device {0}", d.getInstanceNumber()));
			}
		});

		try {

			localDevice.initialize();

			while ( true ) {
				localDevice.sendGlobalBroadcast(new WhoIsRequest());
				System.out.print(".");
				System.out.flush();
				Thread.sleep(5000);
				Thread.yield();
			}

		} catch ( Exception ex ) {
			LOGGER.log(Level.SEVERE, null, ex);
		}

	}

}
