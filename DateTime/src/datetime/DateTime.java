package datetime;


import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public class DateTime {

	@SuppressWarnings( "UseOfSystemOutOrSystemErr" )
	public static void main( String[] args ) {
		
		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		Instant newDate = Instant.now();
		Date oldDate = new Date();
		
		System.out.println("Old date: " + oldDate);
		System.out.println("New date: " + LocalDateTime.ofInstant(newDate, ZoneId.of("Europe/London")).format(DATE_TIME_FORMATTER));
		System.out.println(" +1 date: " + LocalDateTime.ofInstant(newDate, ZoneId.of("Europe/Rome")).format(DATE_TIME_FORMATTER));
		System.out.println("");
		
		File root = new File("/");
		
    	System.out.println("  Total size: " + root.getTotalSpace() + " bytes");
    	System.out.println("Space usable: " + root.getUsableSpace() + " bytes");
    	System.out.println("  Space free: " + root.getFreeSpace() + " bytes");
		System.out.println("    Usable %: " + 100.0 * (double) root.getUsableSpace() / (double) root.getTotalSpace());

	}

}
